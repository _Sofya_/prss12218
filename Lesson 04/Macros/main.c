#include <stdio.h>
#define SIZE 10

int arr[SIZE];

void main()
{
	*(arr + 0) = 0;
	*(arr + 1) = 1;
	*(arr + 2) = *(arr + 0) + *(arr + 1);
	*(arr + 3) = *(arr + 1) + *(arr + 2);
	*(arr + 4) = *(arr + 2) + *(arr + 3);
	*(arr + 5) = *(arr + 3) + *(arr + 4);
	*(arr + 6) = *(arr + 4) + *(arr + 5);
	*(arr + 7) = *(arr + 5) + *(arr + 6);
	*(arr + 8) = *(arr + 6) + *(arr + 7);
	*(arr + 9) = *(arr + 7) + *(arr + 8);

	printf("%d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
		*(arr + 0), *(arr + 1), *(arr + 2),
		arr[3], arr[4], arr[5],
		*(6 + arr), *(7 + arr),
		8[arr], 9[arr]);

	getch();
}