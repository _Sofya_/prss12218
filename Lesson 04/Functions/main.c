#include "main.h"

void main()
{
	askName();
	askAge();
	printCompliment();

	getch();
}

void askName()
{
	printf("What is your name?\n");
	scanf_s("%s", name, SIZE - 1);
}

void askAge()
{
	printf("Hello, %s?\nHow old are you?\n", name);
	scanf_s("%d", &age);
}

void printCompliment()
{
	printf("You look younger, %s.\nI thought you are %d!\n",
		name, age - 5);
}
